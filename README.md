# IP/URL finder with geolocation #

This is a simple web application that find any IP or URL and show its location on the map. In case of any bug email to [thiagohnrster@gmail.com](mailto:thiagohnrster@gmail.com).

### What I used to develop this application? ###

This application use jQuery, and Spirit Framework for responsive design (made by me). See [https://github.com/thiagohnrster/spirit-framework](https://github.com/thiagohnrster/spirit-framework) 

### How to run this application? ###

Open the index.html and enter any IP or URL in the "Query IP / domain" field and click the "Locate" button. To find your location simply click "My location" button and will be displayed the user location on the map and clicking "Reset location" will clear all the location information.

Enjoy! =)
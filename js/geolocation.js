//Update all location information data in the table
function updateLocationDetails(data) {
  var now = new Date();
  //Manipulate and get all the location data
  $('#location_as').html(data.as);
  $('#location_city').html(data.city);
  $('#location_country').html(data.country);
  $('#location_countryCode').html(data.countryCode);
  $('#location_isp').html(data.isp);
  $('#location_lat').html(data.lat);
  $('#location_lon').html(data.lon);
  $('#location_org').html(data.org);
  $('#location_query').html(data.query);
  $('#location_region').html(data.region);
  $('#location_regionName').html(data.regionName);
  $('#location_status').html(data.status);
  $('#location_timezone').html(data.timezone);
  $('#location_zip').html(data.zip);

  //Remove the "empty" class in the table
  $('table').removeClass("empty");
  
  //Display the 'mapContainer' and 'map' DIV
  $('#mapContainer').show();
  $('#map').show();

  //Show other details when clicking the information button in the table
  $('.help').click(function(e) {
    var fieldName = $(e.currentTarget).closest('tr').find('.field_name').text();
    alert('This is your ' + fieldName + ' from ISP ' + data.isp + ' at ' + now);
  });
}

// IP/URL validation
function ipUrlValidation() {
  // IP/URL Regex
  var ipRegex = /^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\.([01]?\\d\\d?|2[0-4]\\d|25[0-5])$/,
    urlRegex = /^((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w[\.\-\+]?){0,62})\w)+)\.(\w{2,6})$/,
    //Error message variable 
    errorMsg = $('<div><span><em></em></span></div>'),
    //Variable that checks for Form error
    isValid = false;
  
  //Applies the class to error message variable 
  $(errorMsg).addClass('demo-error-note');
  
  //Validate the IP/URL field
  $('.query').each(function() {
    if (!ipRegex.test($(this).val()) || !urlRegex.test($(this).val())) {
      if (!isValid) {
        $(this).focus();
        clearLocationDetails();
      }

      $(errorMsg).find('em').html('Invalid query IP / domain!');
      $(errorMsg).clone().hide().fadeIn(200).insertAfter($(this).parents('.demo-grid:last-child'));

      isValid = true;
    }

    if (isValid) {
      return false;
    }
  });
}

//Clear all location information and the form
function clearLocationDetails() {
  updateLocationDetails({
    as: '',
    city: '',
    country: '',
    countryCode: '',
    isp: '',
    lat: '',
    lon: '',
    org: '',
    query: '0.0.0.0',
    region: '',
    regionName: '',
    status: '',
    timezone: '',
    zip: ''
  });

  $('table').addClass('empty');
  $('#mapContainer').hide();
  $('#map').hide();
  $('.query').val('');
  $('.demo-error-note').remove();
}

//Find the user location
function findUserLocation() {
  //Makes an ajax calling that returns all JSON data
  $.ajax({
    type: 'GET',
    url: 'http://ip-api.com/json/',
    success: function(response) {
      updateLocationDetails(response);
    }
  });

  //Gets all the JSON data returning the user location
  $.getJSON('http://ip-api.com/json/', function(data) {
    $('.query').each(function() {
      $(this).val(data.query);
    });
    //Method for initializing the map
    initMap();
    //Method for updating the location information data in the table
    updateLocationDetails(data);
  });

  //Removes the error message
  $('.demo-error-note').remove();
}

//Initialize the application from Handlebars library
function initializePage() {
  window.indexTemplate = $('#index').html();
  window.locationTemplate = $('#locationInfo').html();
  window.indexTemplate = Handlebars.compile(window.indexTemplate);
  window.locationTemplate = Handlebars.compile(window.locationTemplate);

  $('#mainContent').html(window.indexTemplate());
  $('#geoLocationContainer').html(window.locationTemplate({
    as: '',
    city: '',
    country: '',
    countryCode: '',
    isp: '',
    lat: '',
    lon: '',
    org: '',
    query: '0.0.0.0',
    region: '',
    regionName: '',
    status: '',
    timezone: '',
    zip: ''
  }));
}

//Initializes the map
function initMap() {
  //Output message variable 
  var output = document.getElementById('map');

  //Gets all the JSON data returning the IP/URL location on the map
  $.getJSON('http://ip-api.com/json/' + $('.query').val(), function(data) {
    //Display the map if success 
    function success(position) {
      lat = position.coords.latitude;
      lon = position.coords.longitude;
      latlon = new google.maps.LatLng(lat, lon);
      map = document.getElementById('map');

      map.style.width = '100%';
      map.style.height = '340px';

      var myOptions = {
          center: latlon,
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          mapTypeControl: false,
          scrollwheel: false,
          navigationControlOptions: {
            style: google.maps.NavigationControlStyle.SMALL
          }
        },
        map = new google.maps.Map(document.getElementById('map'), myOptions),
        marker = new google.maps.Marker({
          position: latlon,
          map: map,
          title: 'You are here!'
        });
    }

    //Don't display the map if an error occurred
    function error(error) {
      switch (error.code) {
        case error.PERMISSION_DENIED:
          output.innerHTML = 'User denied the request for Geolocation.'
          break;
        case error.POSITION_UNAVAILABLE:
          output.innerHTML = 'Location information is unavailable.'
          break;
        case error.TIMEOUT:
          output.innerHTML = 'The request to get user location timed out.'
          break;
        case error.UNKNOWN_ERROR:
          output.innerHTML = 'An unknown error occurred.'
          break;
      }
    }

    //Checks if geolocation is not supported by the browser
    if (!navigator.geolocation) {
      output.innerHTML = '<p>Geolocation is not supported by your browser</p>';
      return;
    }

    //Shows an output message for loading the map
    output.innerHTML = '<p>Locating...</p>';

    //Position variable for returning the latitude and longitude on the map
    var position = {
      coords: {
        latitude: data.lat,
        longitude: data.lon
      }
    };

    //Finally return the position on the map if success
    success(position);
  });
}

$(function() {
  //Validate the form when clicking the "Locate" button
  $('.btnLocate').on('click', function() {
    //Gets all the JSON data returning the location information
    $.getJSON('http://ip-api.com/json/' + $('.query').val(), function(data) {
      //Check the Query IP/URL field
      $('.query').each(function() {
        //If the Query IP/URL field is empty and the status data in the table is "fail"
        if ($(this).val() === '' || data.status === 'fail' || data.status === '') {
          $(this).focus();
          ipUrlValidation();
          return false;
        } else {
          //Update the location information data
          updateLocationDetails(data);
          //Start the map
          initMap();
        }
      });
    });
  });

  //Calling for initializePage() function
  initializePage();
});